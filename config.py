import os
import base64

def get_env(var):
    try:
        a = os.environ[var]
        return a
    except KeyError:
        return ""

class Development(object):
    FLASK_ENV = "development"
    DEVELOPMENT = True
    DEBUG = True
    CSRF_ENABLED = True

    # The b64 encoding here is mostly so that we can just have the ENV be a single line, and not have to deal with multi-line ENV shenanigans
    PRIVATE_KEY = base64.b64decode(get_env("PRIVATE_KEY_B64"))
    PUBLIC_KEY = base64.b64decode(get_env("PUBLIC_KEY_B64"))
    
    SQLALCHEMY_DATABASE_URI = get_env("DATABASE_URL")

    # Service URLs
    # So theres an interesting problem where you can't set a cookie to have the domain "localhost". In order to get around this, I just set a sub domain I owned to point to localhost, and it fixes the issue for testing. Just make sure to visit http://localhost1.dev.thetwitchy.com during development. The use of my domain here is arbitrary, any other domain can be used for dev as well. If you are doing intra-service testing, then use 127.0.0.2 on localhost2.dev.thetwitchy.com (see other service config for why).
    ROOT_SERVICE_DOMAIN = "dev.thetwitchy.com"

class Production(Development):
    FLASK_ENV = "production"
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SQLALCHEMY_DATABASE_URI = get_env("DATABASE_URL")
    ROOT_SERVICE_DOMAIN = get_env("ROOT_SERVICE_DOMAIN")

    