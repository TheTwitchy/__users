# \_\_users
**DEPRECATED - USE __start STANDALONE INSTEAD**

Underscore authentication and user management. The core component of the \_\_service line.

## Features
* Handles authentication, authorization, and user management for \_\_services.

## Requirements
* A docker instance
* A domain name that you own and control, with the wildcard ('\*') A record pointing to said Docker instance.

## Installation
### Production
We recommend you use Docker Compose for automated deployment.

#### Automated Deployment 
The following example uses the domain `myservices.com` for all deployment, where \_\_users is accessed via `users.myservices.com`, and other services are accessed via `someotherservice.myservices.com`. Replace with your own DNS name as needed.

* `./gen_env.sh` - This generates a custom .env file used by the various services. Edit this file so that your domain is correct. You should keep track of this file somewhere safe.
* `docker-compose up -d`

#### HTTPS Setup
Optional, but recommended. This forces you to access your \_\_services over HTTPS instead of HTTP, thereby securing the connection between you and your server whenever you use \_\_services. There are several ways to implement this, but a reverse proxy is normally the most straightforward option and place to terminate the SSL connection.

### Development
#### VirtualENV
* `pip install virtualenv`
* `virtualenv venv`

#### Install Dependencies
* `source venv/bin/activate`
* `pip install -r requirements.txt`

#### Setup Postgresql
* `sudo apt install postgresql`
* `sudo su postgres`
* `psql`
    * `CREATE DATABASE users_dev;` 
    * `CREATE USER users_user_dev WITH ENCRYPTED PASSWORD 'enter_password_here';`
    * `GRANT ALL PRIVILEGES ON DATABASE users_dev TO users_user_dev;`
    * `\q`
* `python manage.py db init`

##### Updates Models in Database
* `flask db migrate`
    * Only needs to be run on updates to model.py.
* `flask db upgrade`

#### Run Development HTTP Server
* `source dev_env && python manage.py runserver`
* Then visit [the development server](http://127-0-0-1.org.uk:5000/). For an explanation of why this URL is used, see config.py.

#### Misc Development Tasks
##### Update Dependencies
* `pip freeze > requirements.txt`
