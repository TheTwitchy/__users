#!/bin/bash

rm .env
touch .env

# !!!!!!! Change this to your domain !!!!!!!
export ROOT_SERVICE_DOMAIN=yourdomain.com

export PORT=5000

# If you need to do development, enable this, otherwise leave it alone
# echo "__DEVELOPMENT=1" >> .env

# In general you should not need to change anything under this
echo "PORT=$PORT" >> .env
echo "ROOT_SERVICE_DOMAIN=$ROOT_SERVICE_DOMAIN" >> .env

export POSTGRES_USER=users_user
echo "POSTGRES_USER=$POSTGRES_USER" >> .env
export POSTGRES_DB=usersdb
echo "POSTGRES_DB=$POSTGRES_DB" >> .env

# Generate a random password for the database
array=()
for i in {a..z} {A..Z} {0..9}; do
    array[$RANDOM]=$i
done
export POSTGRES_PASSWORD=`printf %s ${array[@]::20} $'\n'`
echo "POSTGRES_PASSWORD=$POSTGRES_PASSWORD" >> .env

# Build the full database URL that the webapp uses
echo "DATABASE_URL=postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@db/$POSTGRES_DB" >> .env

# Generate a random keypair for __users token generation and related services
ssh-keygen -t rsa -b 4096 -m PEM -N "" -f test.key
openssl rsa -in test.key -pubout -outform PEM -out test.key.pub
echo -n "PRIVATE_KEY_B64=" >> .env
cat test.key | base64 -w 0 >> .env
echo >> .env
echo -n "PUBLIC_KEY_B64=" >> .env
cat test.key.pub | base64 -w 0 >> .env
echo >> .env
