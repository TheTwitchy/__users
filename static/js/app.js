"use strict";

// Define the `auth` module
angular.module("authApp", [
    // Dependencies
    "ngRoute",
    "focus-if",
    "userCreate",
    "userView",
    "usersList",
    "login",
    "setup",
    "error404",
    "error500",
]);

angular.
module("authApp").
config(["$routeProvider",
    function config($routeProvider) {
        $routeProvider.
        when("/login", {
            template: "<login></login>"
        }).
        when("/setup", {
            template: "<setup></setup>"
        }).
        when("/users", {
            template: "<users-list></users-list>"
        }).
        when("/users/new", {
            template: "<user-create></user-create>"
        }).
        when("/users/:userId", {
            template: "<user-view></user-view>"
        }).
        when("/error/not-found", {
            template: "<error-404></error-404>"
        }).
        when("/error/unknown", {
            template: "<error-500></error-500>"
        }).
        when("/", {
            template: "<users-list></users-list>"
        }).
        otherwise("/login");
    }
]).filter('checkmark', function() {
    return function(input) {
        return input ? '\u2713' : '';
    };
});;

