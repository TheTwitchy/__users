"use strict";

// Define the `error404` module
angular.module("error404", ["ngRoute"]);

// Register `error404` component, along with its associated controller and template
angular.
module("error404").component("error404", {
    templateUrl: "/static/js/error-404.template.html",
    controller: ["$http", "$routeParams", "$location", "$rootScope", function error404Controller($http, $routeParams, $location, $rootScope) {
        var self = this;
        $rootScope.title = " - Error";

        self.goHome = function (){
            $location.path("/");
        };
    }]
});