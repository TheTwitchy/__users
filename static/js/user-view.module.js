"use strict";

// Define the `userView` module
angular.module("userView", ["ngRoute"]);

// Register `userView` component, along with its associated controller and template
angular.
module("userView").component("userView", {
    templateUrl: "/static/js/user-view.template.html",
    controller: ["$http", "$routeParams", "$location", "$rootScope", function userViewController($http, $routeParams, $location, $rootScope) {
        var self = this;
        $rootScope.title = " - User";
        self.is_working = true;
        self.errors = [];
        self.is_me = $routeParams.userId == "me";

        $http.get("api/users/" + $routeParams.userId).then(
            function(response) {
                self.user = response.data;
                self.user.password1 = "";
                self.user.password2 = "";
                self.errors = [];
                self.is_working = false;
            },
            function(error_response) {
                // Handle errors here
                self.errors = error_response.data.error_msgs;
                self.is_working = false;
            } 
        );

        self.editUserSubmit = function (){
            self.errors = [];
            self.is_working = true;
            $http.put("/api/users/" + $routeParams.userId, self.user).then(
                function(response) {
                    self.errors = [];
                    self.is_working = false;
                },
                function(error_response) {
                    // Handle errors here
                    self.errors = error_response.data.error_msgs;
                    self.is_working = false;
                } 
            );
        };

        self.deleteUserSubmit = function (){
            self.errors = [];
            self.is_working = true;
            $http.delete("/api/users/" + $routeParams.userId).then(
                function(response) {
                    self.errors = [];
                    $location.path("/users");
                },
                function(error_response) {
                    // Handle errors here
                    self.errors = error_response.data.error_msgs;
                    self.is_working = false;
                } 
            );
        };

        this.goList = function (){
            self.is_working = true;
            $location.path("/users");
        };

    }]
});