// This file allows a user to set the theme used across any compatible apps. The theme itself is stored as a part of a user's profile, same as their ID. It is loaded dynamically from the Bootstrap CDN. All themes are pulled form Bootswatch (https://www.bootstrapcdn.com/bootswatch/).
try {
    if (getCookie("__theme")){
        var theme = getCookie("__theme");
        
        var allowed_themes = ["cerulean", "cosmo", "cyborg", "darkly", "flatly", "journal", "litera", "lumen", "lux", "materia", "minty", "pulse", "sandstone", "simplex", "sketchy", "slate", "solar", "spacelab", "superhero", "united", "yeti"];

        if (allowed_themes.indexOf(theme) == -1){
            // The theme is not valid
            loadDefaultTheme();
        }
        else {
            // Theme is valid, load it. 
            loadTheme(theme);
        }
    }
    else {
        loadDefaultTheme();
    }
}
catch(err) {
    // This is mostly called before any authentication takes place.
    loadDefaultTheme();
}

function loadDefaultTheme(){
    loadTheme("cosmo");
}

function loadTheme(themename){ 
    var tag = document.createElement("link");
    tag.setAttribute("href", "https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/" + themename + "/bootstrap.min.css");
    tag.setAttribute("rel", "stylesheet");
    var head = document.getElementsByTagName("head");
    head[0].appendChild(tag);
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}