from app import db
from sqlalchemy.dialects import postgresql
# from sqlalchemy.ext.hybrid import hybrid_method, hybrid_property

from flask_bcrypt import generate_password_hash, check_password_hash

class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(256), unique = True, nullable = False)
    # Password hashing from https://www.patricksoftwareblog.com/password-hashing/
    _password = db.Column(db.LargeBinary(120), nullable = False)
    full_name = db.Column(db.String(256), nullable = False)
    is_admin = db.Column(db.Boolean, default=False, nullable = False)
    permissions = db.Column(db.String(1024), nullable = True)
    theme = db.Column(db.String(40), nullable = True)
    
    def __init__(self, email, full_name, cleartext_password, is_admin = False, permissions = "", theme = "cosmo"):
        self.email = email
        self.full_name = full_name
        self.permissions = permissions
        self._password = generate_password_hash(cleartext_password)
        self.is_admin = is_admin
        self.theme = theme

    def to_dict(self):
        user = {}
        user['id'] = self.id
        user['email'] = self.email
        user['name'] = self.full_name
        user['is_admin'] = self.is_admin
        user['permissions'] = self.permissions
        user["theme"] = self.theme
        
        return user


    def is_correct_password(self, plaintext_password):
        return check_password_hash(self._password, plaintext_password)

    def set_password(self, new_plaintext_password):
        self._password = generate_password_hash(new_plaintext_password)
